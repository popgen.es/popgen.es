OUTPUT=~/public_html/popgen.es

all:
	python3 -Wdefault make_website.py $(OUTPUT)
	@echo Done.

offline:
	python3 -Wdefault make_website.py $(OUTPUT) --offline

clean: cleanish
	rm -rf _succession_cache

cleanish:
	rm -rf $(OUTPUT)
	rm -rf _build

.PHONY : all cleanish clean refresh
