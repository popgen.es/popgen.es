#!/usr/bin/python3

import logging, os, tempfile
from pathlib import Path
from warnings import warn

import tomli

import jinjagen  # type: ignore

from epijats import EprinterConfig, Eprint, Webstract
from epijats.jinja import style_template_loader
from hidos.dsi import BaseDsi
from hidos.cache import SuccessionCache
from hidos.util import LOG


WEBSRC = Path("websrc")
BUILD = Path("_build")
with open("config.toml", "rb") as f:
    CONFIG = tomli.load(f)
SUCC_CACHE = Path("_succession_cache")
SWHA_BEARER_TOKEN_PATH = "swh-token.txt"

assert WEBSRC.is_dir()


class SuccessionFacade:
    def __init__(self, succession, docs):
        self.succession = succession
        self._docs = [doc.facade for doc in docs.values()]

    @property
    def dsi(self) -> str:
        return str(self.succession.dsi)

    @property
    def revision(self):
        return self.succession.revision

    @property
    def editions(self):
        return self._docs


class SuccessionPages:

    def __init__(self, gen, config, succ_facade):
        self.gen = gen
        self.config = config
        self._facade = succ_facade

    def make_edition(self, edition, doc, portal):
        ctx = dict(
            doc=doc.facade if doc else None,
            portal=portal,
            dsi_domain="perm.pub",
            path_edid=edition.edid,
            succession=self._facade,
        )
        subpath = Path(str(edition.dsi))
        self.gen.gen_file("_edition.html.jinja", subpath / "index.html", ctx)
        if edition.snapshot:
            self.make_pdf(doc, self.gen.dest / subpath / "article.pdf")

    def make_pdf(self, doc, pdf_dest):
        if not pdf_dest.exists():
            print(pdf_dest)
            with tempfile.TemporaryDirectory() as tmpdir:
                eprint = Eprint(doc, tmpdir, self.config)
                eprint.make_pdf(pdf_dest)


def make_docs(succession, dates):
    ret = dict()
    for e in succession.root.all_subeditions():
        if e.snapshot:
            doc = Webstract.from_edition(e, BUILD / str(e.dsi))
            archive_date = dates.get(e.edid)
            if archive_date:
                doc["archive_date"] = archive_date.date()
            else:
                warn(f"Missing archive date for dsi:{succession.dsi}/{e.edid}")
            ret[e.edid] = doc
    return ret


def make_succession_pages(gen, config, succession, portal, dates): 
    docs = make_docs(succession, dates)
    facade = SuccessionFacade(succession, docs)
    pages = SuccessionPages(gen, config, facade)
    for e in succession.root.all_subeditions() + [succession.root]:
        flow = e.flow_edition()
        doc = docs[flow.edid] if flow else None
        pages.make_edition(e, doc, portal)
    gen.gen_file(
        "_about.html.jinja",
        Path(facade.dsi) / "about" / "index.html",
        dict(succession=facade),
    )


def make_website(dest_dir, offline: bool):
    with open(SWHA_BEARER_TOKEN_PATH) as f:
        os.environ["SWHA_BEARER_TOKEN"] = f.read().strip()
    cache = SuccessionCache(SUCC_CACHE, offline=offline)
    signed_dsi = set()
    gen = jinjagen.JinjaGenerator(WEBSRC, dest_dir)
    gen.hook_module("jinjagenadd")
    gen.env.loader.loaders.append(style_template_loader())
    Eprint.copy_static_dir(gen.dest / "static")
    config = EprinterConfig(dsi_domain="perm.pub")
    config.embed_web_fonts = False

    for key, sx in CONFIG["successions"].items():
        dsi = BaseDsi(key)
        print(dsi)
        ds = cache.get(dsi)
        if ds.is_signed:
            signed_dsi.add(dsi)
        dates = cache.archive_dates(ds)
        make_succession_pages(gen, config, ds, sx.get("portal"), dates)

    gen.env.globals.update(dict(dsis=sorted(signed_dsi)))
    gen.gen_site()


if __name__ == "__main__":
    import argparse

    LOG.setLevel(logging.INFO)
    LOG.addHandler(logging.StreamHandler())

    parser = argparse.ArgumentParser(description="Generate website")
    parser.add_argument("output", help="Path to ouput directory")
    parser.add_argument(
        "--offline", action="store_true", help="Make offline, use cache only"
    )
    args = parser.parse_args()
    make_website(Path(args.output), offline=args.offline)
